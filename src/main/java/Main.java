import org.junit.runner.JUnitCore;

public class Main
{
    public static void main(String[] args) {
        JUnitCore.main("test.TestHostnameRetrieval", "test.TestLdaps");
    }
}
